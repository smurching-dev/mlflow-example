import inspect
import os
import sys

import pyspark
from pyspark.sql import SparkSession

print("sys.path: %s" % sys.path)
print("Using pyspark from %s" % inspect.getfile(pyspark))

spark = SparkSession.builder.getOrCreate()
for conf in spark.sparkContext.getConf().getAll():
    print(conf)
