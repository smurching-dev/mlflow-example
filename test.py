from __future__ import print_function

# import time
import inspect
import sys
import time
print("(stdout): Running Python code with module search path %s" % sys.path)
sys.stderr.write("(stderr) Running Python code with module search path %s\n" % sys.path)

import pyspark
from pyspark.sql import SparkSession

from mlflow import log_metric, log_parameter, log_output_files

if __name__ == "__main__":
    print("Running {}".format(sys.argv[0]))
    spark = SparkSession.builder.getOrCreate()
    df = spark.range(3)
    print("Created dataframe with rows: %s" % df.collect())
    print("Using PySpark from %s" % inspect.getfile(pyspark))
    log_parameter("param1", 5)
    log_metric("foo", 5)
    log_metric("foo", 6)
    log_metric("foo", 7)
    log_output_files("outputs")
    print("Sleeping for 12 hrs in chunks of 1 hr each")
    for i in range(12):
        time.sleep(3600)
        print("Slept %s hours" % (i + 1))
